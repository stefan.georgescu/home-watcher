
# Home Watcher - Work in Progress

Personal project with the intent of monitoring various network statistics within the local network. Possibilities to expand it further to security cameras, temperature and humidity sensors, and pretty much everything that can be connected to a Raspberry Pi.

## Structure

The project is represented by an Angular application which is deployed on a Raspberry Pi and exposed in the local network via a DNS server hosted on the same Pi. More documentation will be available once the project is fully implemented since now it is still work in progress.