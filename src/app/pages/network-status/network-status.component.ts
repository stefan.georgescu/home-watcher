import { NetworkStatistics } from './../../@core/model/network-statistics.model';
import { TimePeriod } from './../../@core/model/time-period.model';
import { MultipleYvaluesChartData } from '../../@core/model/multiple-yvalues-chart-data.model';

import { LiveNetworkData } from '../../@core/model/live-network-data.model';
import { takeWhile, switchMap } from 'rxjs/operators';
import { NbThemeService } from '@nebular/theme';
import { Subscription, interval } from 'rxjs';
import { Component, OnDestroy, OnInit, Input } from '@angular/core';
import { NetworkData, NetworkStatisticsType } from '../../@core/data/network';

@Component({
  selector: 'ngx-hw-home',
  templateUrl: './network-status.component.html',
  styleUrls: ['network-status.component.scss'],
})
export class NetworkStatusComponent implements OnDestroy, OnInit {
  private alive = true;
  public period: string = TimePeriod.day;

  pingChartData: MultipleYvaluesChartData;
  speedChartData: MultipleYvaluesChartData;
  packetLossChartData: MultipleYvaluesChartData;

  livePingData: LiveNetworkData;
  liveSpeedData: LiveNetworkData;

  pingIntervalSubscription: Subscription;
  speedIntervalSubscription: Subscription;

  shouldUpdatePing: Boolean = false;
  shouldUpdateSpeed: Boolean = false;

  constructor(private networkService: NetworkData) { }

  ngOnInit() {
    this.getAllChartData(this.period);
    this.getSummaryStatisticsData();
  }

  private getAllChartData(period: string) {
    this.networkService.getFullStatisticsData(NetworkStatisticsType.PING, TimePeriod.day).subscribe(
      (netwrokStatistics: NetworkStatistics) => {
        this.pingChartData = netwrokStatistics.getMultipleYvaluesChartData();
    });

    this.networkService.getFullStatisticsData(NetworkStatisticsType.SPEED, TimePeriod.day).subscribe(
      (netwrokStatistics: NetworkStatistics) => {
        this.speedChartData = netwrokStatistics.getMultipleYvaluesChartData();
    });

    this.networkService.getFullStatisticsData(NetworkStatisticsType.PACKET_LOSS, TimePeriod.day).subscribe(
      (netwrokStatistics: NetworkStatistics) => {
        this.packetLossChartData = netwrokStatistics.getMultipleYvaluesChartData();
    });
  }

  private getSummaryStatisticsData() {
    this.networkService.getSummaryStatisticsData(NetworkStatisticsType.PING).subscribe(
      (pingSummaryStatistics: LiveNetworkData) => {
        this.livePingData = pingSummaryStatistics;

        this.startReceivingLiveStatistics(NetworkStatisticsType.PING);
    });

    this.networkService.getSummaryStatisticsData(NetworkStatisticsType.SPEED).subscribe(
      (speedSummaryStatistics: LiveNetworkData) => {
        this.liveSpeedData = speedSummaryStatistics;

        this.startReceivingLiveStatistics(NetworkStatisticsType.SPEED);
    });
  }

  private startReceivingLiveStatistics(type: NetworkStatisticsType) {
    // TODO - create subs in a map.

    switch (type) {
      case NetworkStatisticsType.PING:
        if (this.pingIntervalSubscription) {
          this.pingIntervalSubscription.unsubscribe();
        }

        this.pingIntervalSubscription = interval(5000)
        .pipe(
          takeWhile(() => this.alive),
          switchMap(() => this.networkService.getLiveSummaryStatisticsData(type)),
        )
        .subscribe((updateData: LiveNetworkData) => {
          this.livePingData = updateData;
          this.shouldUpdatePing = !this.shouldUpdatePing; // TODO - find a way of removing this hack
        });
        break;

      case NetworkStatisticsType.SPEED:
        if (this.speedIntervalSubscription) {
          this.speedIntervalSubscription.unsubscribe();
        }

        this.speedIntervalSubscription = interval(5000)
        .pipe(
          takeWhile(() => this.alive),
          switchMap(() => this.networkService.getLiveSummaryStatisticsData(type)),
        )
        .subscribe((updateData: LiveNetworkData) => {
          this.liveSpeedData = updateData;
          this.shouldUpdateSpeed = !this.shouldUpdateSpeed; // TODO - find a way of removing this hack
        });
        break;

      default:
        break;
    }
  }

  setPeriodAndGetChartData(value: string, chart: string): void {
    // THIS IS THE PLACE WHERE WE UPDATE CHART DATA.
  }

  ngOnDestroy() {
    this.alive = false;
  }
}
