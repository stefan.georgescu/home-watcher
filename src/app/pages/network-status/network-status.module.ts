// tslint:disable-next-line:max-line-length
import { PeriodSelectHeaderComponent } from './multiple-xasis-chart/period-select-header/period-select-header.component';
import { MultipleYvaluesChartComponent } from './multiple-xasis-chart/multiple-yvalues-chart.component';
import { RealTimeChartComponent } from './real-time-chart/real-time-chart.component';
import { NetworkStatusComponent } from './network-status.component';
import { NgModule } from '@angular/core';
import { NgxEchartsModule } from 'ngx-echarts';
import {
  NbCardModule,
  NbSelectModule,
  NbIconModule,
} from '@nebular/theme';

import { ThemeModule } from '../../@theme/theme.module';

@NgModule({
  imports: [
    ThemeModule,
    NbCardModule,
    NbSelectModule,
    NbIconModule,
    NgxEchartsModule,
  ],
  declarations: [
    NetworkStatusComponent,
    RealTimeChartComponent,
    MultipleYvaluesChartComponent,
    PeriodSelectHeaderComponent,
  ],
})
export class NetworkStatusModule {}
