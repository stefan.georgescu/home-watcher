import { Component } from '@angular/core';

@Component({
  selector: 'ngx-hw-home',
  templateUrl: './home.component.html',
  styleUrls: ['home.component.scss'],
})
export class HomeComponent {

  locations: string[] = ['Pitesti', 'Cluj-Napoca', 'Bucharest'];
}
