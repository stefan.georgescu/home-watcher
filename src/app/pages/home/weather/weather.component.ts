import { WeatherInformation } from './../../../@core/data/weather';
import { WeatherService } from './../../../@core/mock/weather.service';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ngx-hw-weather',
  styleUrls: ['./weather.component.scss'],
  templateUrl: './weather.component.html',
})

export class WeatherComponent implements OnInit {

  @Input() location: string;

  weatherInformation: WeatherInformation;

  constructor(private weatherService: WeatherService) { }

  ngOnInit() {
    this.weatherService.getData(this.location).subscribe((receivedWeatherInformation: WeatherInformation) => {
      this.weatherInformation = receivedWeatherInformation;
    });
  }

}
