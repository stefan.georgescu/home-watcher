import { WeatherComponent } from './weather/weather.component';
import { HomeComponent } from './home.component';
import { NgModule } from '@angular/core';
import {
  NbCardModule,
  NbIconModule} from '@nebular/theme';

import { ThemeModule } from '../../@theme/theme.module';

@NgModule({
  imports: [
    ThemeModule,
    NbCardModule,
    NbIconModule,
  ],
  declarations: [
    HomeComponent,
    WeatherComponent,
  ],
})
export class HomeModule { }
