import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Home',
    icon: 'home-outline',
    link: '/pages/home',
    home: true,
  },
  {
    title: 'NETWORKING',
    group: true,
  },
  {
    title: 'Network Status',
    icon: 'wifi-outline',
    link: '/pages/network-status',
  },
];
