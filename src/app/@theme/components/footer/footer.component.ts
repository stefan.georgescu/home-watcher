import { Component } from '@angular/core';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <p>You are the most important product you will ever develop.</p>
  `,
})
export class FooterComponent {
}
