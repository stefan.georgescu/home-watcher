import { WeatherInformation, WeatherType } from './../data/weather';
import { of as observableOf,  Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { WeatherData } from '../data/weather';

@Injectable()
export class WeatherService extends WeatherData {

  private getMockWeatherData(location: string): WeatherInformation {
    return {
      location: location,
      date: 'Fri 24 April',
      temperature: 22,
      minTemperature: 10,
      maxTemperature: 25,
      windSpeed: 3,
      humidity: 85.3,
      type: WeatherType.sun,
      forecast: [
        {
          temperature: 17,
          type: WeatherType.cloud,
          day: 'SUN',
        },
        {
          temperature: 19,
          type: WeatherType.sun,
          day: 'MON',
        },
        {
          temperature: 22,
          type: WeatherType.rain,
          day: 'TUE',
        },
        {
          temperature: 21,
          type: WeatherType.partly_sunny,
          day: 'WED',
        },
      ],
    };
  }

  getData(location: string): Observable<WeatherInformation> {
    return observableOf(this.getMockWeatherData(location));
  }
}
