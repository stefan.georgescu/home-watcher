import { NetworkStatistics, NetworkStatisticsData } from './../model/network-statistics.model';
import { TimePeriod } from './../model/time-period.model';
import { LiveNetworkData } from '../model/live-network-data.model';
import { NetworkStatisticsType } from './../data/network';
import { Injectable } from '@angular/core';
import { of as observableOf, Observable } from 'rxjs';
import { NetworkData } from '../data/network';

@Injectable()
export class NetworkService extends NetworkData {
  private currentPingDate: Date = new Date();
  private currentSpeedDate: Date = new Date();
  private ONE_DAY = 24 * 3600 * 1000;

  private currentPingSummaryData: LiveNetworkData;
  private currentSpeedSummaryData: LiveNetworkData;

  getFullStatisticsData(type: NetworkStatisticsType, period: TimePeriod): Observable<NetworkStatistics> {
    switch (type) {
      case NetworkStatisticsType.PING:
        return observableOf(this.getMockPingNetworkStatisticsData(period));

      case NetworkStatisticsType.PACKET_LOSS:
        return observableOf(this.getMockPacketLossData(period));

      case NetworkStatisticsType.SPEED:
        return observableOf(this.getMockSpeedData(period));

      default:
        break;
    }
  }

  getSummaryStatisticsData(type: NetworkStatisticsType): Observable<LiveNetworkData> {
    switch (type) {
      case NetworkStatisticsType.PING:
        return observableOf(this.getMockPingSummaryStatisticsData());

      case NetworkStatisticsType.SPEED:
        return observableOf(this.getMockSpeedSummaryStatisticsData());

      default:
        break;
    }
  }
  getLiveSummaryStatisticsData(type: NetworkStatisticsType): Observable<LiveNetworkData> {
    switch (type) {
      case NetworkStatisticsType.PING:
        return observableOf(this.getMockPingLiveSummaryStatisticsData());

      case NetworkStatisticsType.SPEED:
        return observableOf(this.getMockSpeedLiveSummaryStatisticsData());

      default:
        break;
    }
  }

  private getMockPingNetworkStatisticsData(period: TimePeriod): NetworkStatistics {
    const networkDataMin: NetworkStatisticsData = {
      networkData: [
        {timestamp: '10', value: 19.629},
        {timestamp: '11', value: 39.803},
        {timestamp: '12', value: 30.108},
        {timestamp: '13', value: 17.795},
        {timestamp: '14', value: 27.552},
        {timestamp: '15', value: 32.451},
        {timestamp: '16', value: 26.327},
      ],
    };

    const networkDataAvg: NetworkStatisticsData = {
      networkData: [
        {timestamp: '10', value: 50.830},
        {timestamp: '11', value: 77.056},
        {timestamp: '12', value: 38.278},
        {timestamp: '13', value: 35.103},
        {timestamp: '14', value: 230.712},
        {timestamp: '15', value: 288.878},
        {timestamp: '16', value: 8.311},
      ],
    };

    const networkDataMax: NetworkStatisticsData = {
      networkData: [
        {timestamp: '10', value: 83.616},
        {timestamp: '11', value: 107.598},
        {timestamp: '12', value: 46.978},
        {timestamp: '13', value: 49.414},
        {timestamp: '14', value: 963.794},
        {timestamp: '15', value: 1019.755},
        {timestamp: '16', value: 52.430},
      ],
    };

    const networkStatistic = new NetworkStatistics();
    networkStatistic.data['Min'] = networkDataMin;
    networkStatistic.data['Avg'] = networkDataAvg;
    networkStatistic.data['Max'] = networkDataMax;

    return networkStatistic;
  }

  private getMockPacketLossData(period: TimePeriod): NetworkStatistics {
    const networkData: NetworkStatisticsData = {
      networkData: [
        {timestamp: '10', value: 0},
        {timestamp: '11', value: 0},
        {timestamp: '12', value: 0},
        {timestamp: '13', value: 0},
        {timestamp: '14', value: 0},
        {timestamp: '15', value: 20},
        {timestamp: '16', value: 0},
      ],
    };

    const networkStatistic = new NetworkStatistics();
    networkStatistic.data['Loss'] = networkData;

    return networkStatistic;
  }

  private getMockSpeedData(period: TimePeriod): NetworkStatistics {
    const downloadData: NetworkStatisticsData = {
      networkData: [
        {timestamp: '10', value: 20},
        {timestamp: '11', value: 21},
        {timestamp: '12', value: 22},
        {timestamp: '13', value: 23},
        {timestamp: '14', value: 20},
        {timestamp: '15', value: 24},
        {timestamp: '16', value: 18},
      ],
    };

    const uploadData: NetworkStatisticsData = {
      networkData: [
        {timestamp: '10', value: 10},
        {timestamp: '11', value: 30},
        {timestamp: '12', value: 28},
        {timestamp: '13', value: 16},
        {timestamp: '14', value: 19},
        {timestamp: '15', value: 20},
        {timestamp: '16', value: 22},
      ],
    };

    const networkStatistic = new NetworkStatistics();
    networkStatistic.data['Upload'] = uploadData;
    networkStatistic.data['Download'] = downloadData;

    return networkStatistic;
  }

  private getMockPingSummaryStatisticsData(): LiveNetworkData {
    this.currentPingSummaryData = new LiveNetworkData();
    this.currentPingSummaryData.networkData =
    Array.from(Array(150)).map(_ => this.generateRandomLiveNetworDataPoint(23, 100, 3, NetworkStatisticsType.PING));

    this.currentPingSummaryData.latest = 52.430;
    this.currentPingSummaryData.average = 400;

    return this.currentPingSummaryData;
  }

  private getMockSpeedSummaryStatisticsData(): LiveNetworkData {
    this.currentSpeedSummaryData = new LiveNetworkData();
    this.currentSpeedSummaryData.networkData =
    Array.from(Array(150)).map(_ => this.generateRandomLiveNetworDataPoint(5, 15, 1, NetworkStatisticsType.SPEED));

    this.currentSpeedSummaryData.latest = 16;
    this.currentSpeedSummaryData.average = 18;

    return this.currentSpeedSummaryData;
  }

  private getMockPingLiveSummaryStatisticsData(): LiveNetworkData {
    const length = 150;
    const newValue = this.generateRandomLiveNetworDataPoint(23, 100, 3, NetworkStatisticsType.PING);

    this.currentPingSummaryData.latest = +newValue.value[1];
    this.currentPingSummaryData.average =
    (this.currentPingSummaryData.average * length + +newValue.value[1]) / (length + 1);
    this.currentPingSummaryData.networkData.shift();
    this.currentPingSummaryData.networkData.push(newValue);

    return this.currentPingSummaryData;
  }

  private getMockSpeedLiveSummaryStatisticsData(): LiveNetworkData {
    const length = 150;
    const newValue = this.generateRandomLiveNetworDataPoint(5, 15, 1, NetworkStatisticsType.SPEED);

    this.currentSpeedSummaryData.latest = +newValue.value[1];
    this.currentSpeedSummaryData.average =
    (this.currentSpeedSummaryData.average * length + +newValue.value[1]) / (length + 1);

    const newArray = this.currentSpeedSummaryData.networkData;
    newArray.shift();
    newArray.push(newValue);

    this.currentSpeedSummaryData.networkData = [...newArray];
    return this.currentSpeedSummaryData;
  }

  private generateRandomLiveNetworDataPoint(mmin: number, mmax: number, skew: number, type: NetworkStatisticsType) {
    let dateToUse: Date;

    if (type === NetworkStatisticsType.PING) {
      this.currentPingDate = new Date(+this.currentPingDate + this.ONE_DAY);
      dateToUse = this.currentPingDate;
    } else {
      this.currentSpeedDate = new Date(+this.currentSpeedDate + this.ONE_DAY);
      dateToUse = this.currentSpeedDate;
    }

    const currentValue = this.getRandomValue(mmin, mmax, skew);

    return {
      value: [
        [
          dateToUse.getFullYear(),
          dateToUse.getMonth(),
          dateToUse.getDate(),
        ].join('/'),
        Math.round(currentValue),
      ],
    };
  }

  private getRandomValue(mmin: number, mmax: number, skew: number): number {
      let u = 0, v = 0;
      while (u === 0) u = Math.random();
      while (v === 0) v = Math.random();
      let num = Math.sqrt( -2.0 * Math.log( u ) ) * Math.cos( 2.0 * Math.PI * v );

      num = num / 10.0 + 0.5;
      if (num > 1 || num < 0) num = this.getRandomValue(mmin, mmax, skew);
      num = Math.pow(num, skew);
      num *= mmax - mmin;
      num += mmin;
      return num;
  }
}
