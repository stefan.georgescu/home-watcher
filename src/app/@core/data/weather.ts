import { Observable } from 'rxjs';

export interface WeatherInformation {
  location: string;
  date: string;
  temperature: number;
  minTemperature: number;
  maxTemperature: number;
  windSpeed: number;
  humidity: number;
  type: WeatherType;
  forecast: WeatherForecast[];
}

export enum WeatherType {
  cloud = 'CLOUD',
  sun = 'SUN',
  rain = 'RAIN',
  partly_sunny = 'PARTLYSUNNY',
}

export interface WeatherForecast {
  temperature: number;
  type: WeatherType;
  day: string;
}

export abstract class WeatherData {
  abstract getData(location: string): Observable<WeatherInformation>;
}
