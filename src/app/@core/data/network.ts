import { NetworkStatistics } from './../model/network-statistics.model';
import { TimePeriod } from './../model/time-period.model';
import { LiveNetworkData } from '../model/live-network-data.model';
import { Observable } from 'rxjs';

export enum NetworkStatisticsType {
  PING,
  PACKET_LOSS,
  SPEED,
}

export abstract class NetworkData {
  abstract getFullStatisticsData(type: NetworkStatisticsType, period: TimePeriod): Observable<NetworkStatistics>;
  abstract getSummaryStatisticsData(type: NetworkStatisticsType): Observable<LiveNetworkData>;
  abstract getLiveSummaryStatisticsData(type: NetworkStatisticsType): Observable<LiveNetworkData>;
}
