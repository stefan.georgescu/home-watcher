import { TimePeriod } from './time-period.model';

export class TimePeriodUtil {
  static getAllAsString(): string[] {
    return Object.keys(TimePeriod).map(key => TimePeriod[key]);
  }
}
