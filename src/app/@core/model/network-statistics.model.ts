import { MultipleYvaluesChartData } from './multiple-yvalues-chart-data.model';

export interface NetworkStatisticsData {
  networkData: {timestamp: string, value: number}[];
}

export class NetworkStatistics {
  data: { [label: string]: NetworkStatisticsData } = {};

  public getMultipleYvaluesChartData(): MultipleYvaluesChartData {
    const chartData: MultipleYvaluesChartData = new MultipleYvaluesChartData();

    chartData.setLegend(Object.keys(this.data));
    chartData.setXaxisLabels(this.data[chartData.getLegend()[0]].networkData.map(el => el.timestamp));
    chartData.getLegend().forEach(element => {
      chartData.dataSeries.push(this.data[element].networkData.map(el => el.value));
    });

    return chartData;
  }
}
