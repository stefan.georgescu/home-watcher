
export class LiveNetworkData {
  networkData: { value: (string | number)[] }[];
  latest: number;
  average: number;
}
