
export interface ChartDataSeries {
  name: string;
  type: string;
  smooth: boolean;
  data: number[];
}

export class MultipleYvaluesChartData {
  private xLabels: string[] = [];
  public dataSeries: number[][] = [];
  private legend: string[] = [];

  public setLegend(legend: string[]) {
    this.legend = legend;
  }

  public getLegend(): string[] {
    return this.legend;
  }

  public setXaxisLabels(labels: string[]) {
    this.xLabels = labels;
  }

  public getXaxisLabels(): string[] {
    return this.xLabels;
  }

  public getDataSeries(): ChartDataSeries[] {
    const series = [];
    for (let i = 0; i < this.legend.length; i++) {
      const seriesObj = {
          name: this.legend[i],
          type: 'line',
          smooth: true,
          data: this.dataSeries[i],
      };
      series.push(seriesObj);
    }
    return series;
  }

  public getPointerTextForDataPoint(dataPoint: any): string {
    // see echarts documentation for dataPoint structure
    // https://echarts.apache.org/en/option.html#axisPointer.label.formatter

    let ans = '';
    ans += dataPoint.value + '\n';
    for (let i = 0; i < this.legend.length; i++) {
      ans += this.legend[i] + (dataPoint.seriesData.length ? '：' + dataPoint.seriesData[i].data : '') + '\n';
    }
    return ans;
  }
}
