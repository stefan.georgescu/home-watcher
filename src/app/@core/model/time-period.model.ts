export enum TimePeriod {
  day = 'Day',
  week = 'Week',
  month = 'Month',
}
